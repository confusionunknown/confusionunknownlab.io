---
title: "PDA Nonsense"
date: 2023-11-18T15:48:09Z
draft: false
---
 - [OPL Programming on Psion EPOC16 Devices]({{<ref "/posts/epoc16opl">}})

I'm working on some projects related to awesome 90s PDAs (personal digital assistants), i'll update this page when I have more to show.

## Downloads:
Psion Series 3 (maybe 3a,3mx) Battery cover replacement:
[Download]({{<ref "/downloads/3battery">}})
