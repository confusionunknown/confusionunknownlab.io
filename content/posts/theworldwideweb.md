+++
title = "The World Wide Web"
date = "2023-11-18T16:40:42Z"
author = "ConfusionUnknown"
authorTwitter = "" #do not include @
cover = ""
tags = ["thoughts", "web"]
keywords = []
description = "Hiya people, This is my website on the world wide web"
showFullContent = false
+++

Hiya people!

This is my page on the world wide web. I'm trying out GitLab pages and Hugo, two new things at once!

Not much to see here at the moment, check back later.


*This page is best viewed in netscape navigator 4 on a 300 baud modem.*

*Pentium 2 recommended*
