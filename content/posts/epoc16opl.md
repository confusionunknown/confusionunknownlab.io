+++
title = "EPOC16 OPL Programming"
date = "2023-11-25T23:04:15Z"
author = "ConfusionUnknown"
authorTwitter = "" #do not include @
cover = ""
tags = ["pda", "development"]
keywords = ["", ""]
description = "Psion PDAs prior to the Series 5 ran a multitasking operating system known as EPOC16..."
showFullContent = false
color = "" #color from the theme settings
+++
Psion PDAs prior to the Series 5 ran a multitasking operating system known as EPOC16, and every device with EPOC16 can run programs developed in a language called OPL, that is built right into every device.

This makes it handy for quickly automating lengthy calculations or other tasks without needing to use a computer, it has a syntax that I would best describe as BASIC-like.
This is something I'm not entirely keen on, but given the vintage of the language, It's not a surprise.

I find the graphical instructions particularly interesting, as they allow for the creation of window driven GUI applications on the device itself, in 1991!

A few simple lines at the beginning transform the OPL program into an "OPA", an installable file that adds the application to the menu,
With a suitable serial cable, you can even transfer a bitmap image to be used as an icon!

While I can't find a reliable source for this, I'm pretty sure that many Psion PDAs came bundled with a book on programming in the OPL language, allowing people to write helpful utilities without needing to buy a book or download documentation from the web.

I'll have some OPL applications I've written posted up here shortly, with source code of course!
