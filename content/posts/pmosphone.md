+++
title = "I installed Linux on my Smartphone"
date = "2024-02-19T22:14:00Z"
author = "ConfusionUnknown"
tags = ["linux", "mobile"]
description = "Adventures replacing Android with something better"
showFullContent = false
readingTime = true
+++


It's time to get a new smartphone, and I'm tired of the crap that most mobile phone vendors are trying to do, iOS and Android devices both.

I chose the OnePlus 6 as it runs on the Snapdragon 845, one of the best supported by the mobile linux distro [postmarketOS](https://postmarketos.org/); The OnePlus 6 is one of the best supported devices running on the Snapdragon 845 SoC, so after some research, that's what I decided to buy

## How flashing works
Flashing postmarketOS is actually relatively simple, download the relevant files for your device, update OxygenOS to the latest version, then use ADB to flash postmarketOS, much like any other custom android ROM (You have to erase the device tree partition first, but it's pretty simple)
This is all detailed in the documentation, and for anyone looking to do this themselves, I'd recommend they look there to find what isn't going to work (For this device, it's currently the Camera and Fast USB Charging)

## About those missing features
I hinted at this earlier, but I feel it is important to mention the things that currently don't work on this device, the biggest one being Camera function. The camera module in question (IMX519), needs a driver, and support for the sensor needs to be added - support for which is being worked on, but at the time of writing, not available yet.

GPS is another rough patch, with it being available only if you install a specific OxygenOS version prior to installing postmarketOS.

As annoying as it's going to be, I think I can live without a camera and fast charging, I don't use my phone all that much, and that's the only reason I'm doing this experiment on my main device.

## It's time to Flash postmarketOS
After my OnePlus 6 arrived, I began to install postmarketOS, the first step of which is some preparation in android.

Once the android setup was complete, the next step is to update both slots of the device to the last version of OxygenOS, To do this, you first ensure that the device says it is up to date, then, you manually download the OTA zip and install that locally, this ensure that both slots are on the same version.

After enabling OEM unlocking and booting into fastboot mode - I was ready, you first unlock the bootloader, then erase the device tree partition. Next, you flash the boot and userdata partitions, then just run "fastboot reboot", and hope nothing went wrong.

## First impressions
GNOME Mobile looks great, and it's really nice to use - This is what struck me after it booted for the first time. Unfortunately, my first impressions were somewhat soured by a regression in dnsmasq, causing all DNS resolution to fail. I asked about this in the IRC, and it appears to be something that just happened, and the issue has now been fixed, though in the stable images you still need to do a workaround to download the patched version

After getting that fixed up, I installed some applications: Tuba, for connecting to the fediverse - and Polari, for IRC

Tuba was a bit annoying to get working, I could enter the address of my server, but it would then try to open the browser, and inform me that I didn't have any installed apps that could open the link. In the end, opening that app from a terminal, waiting for the page and opening the link from the terminal logs did the trick.

(This issue of not opening links continued to links in posts, but it has now started working, I'm not sure why)

Polari worked flawlessly, adding networks and rooms went off without a hitch.

An app that surprised me was Amberol, I'd been using it for a while on my linux desktop, and it looks great, this continues to mobile.

## Should you do this?
Probably not, for most people, and I'm not saying that running mainline Linux on a phone isn't cool or useful or good, it's just that it isn't complete enough for most people, It's just usable enough for me and because of that I'm using it.

However - even if you don't go and use it, you should watch the projects involved, postmarketOS, Mobian etc, for news, and to see if it becomes usable for you.