---
title: "Psion series 3 replacement battery cover"
date: 2023-12-27T22:59:47Z
draft: false
description: "3D Printable Replacement part for an aging PDA"
lastmod: 2023-12-28
---
Replacement battery cover for the Psion Series 3 PDA devices.

 - [STL File](/downloads/3battery.stl) (For 3D Printing)
 - [FreeCAD File](/downloads/3battery.FCStd) (For Editing)

These files have a license associated with them:

This work is licensed under CC BY-SA 4.0. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/
